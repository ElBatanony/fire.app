# fire.app

fire.app is a helper package for deploying (mounting and unmounting) web single-page apps.

This package is used in conjunction with the @ijl/cli package.

This package is built for the Enterprise Programming on JavaScript (Advanced) course at Innopolis University.