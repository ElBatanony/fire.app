export class Apps extends Map {

    constructor(apps) {
      super()
      this.merge(apps)
    }
  
    merge(apps) {
      (Object.entries(
        apps
      ).forEach(([path, options]) =>
        this.set(path, options)
      ))
    }
  
    startWithPath(path, subPath) {
      if (!subPath) {
        return true;
      }
  
      const pathItem = String(path).split('/');
      const subPathItems = String(subPath).split('/')
  
      return subPathItems.reduce(
        (memo, appItem, index) => memo && pathItem[index] === appItem,
        true
      )
    }
  
    findApp = (path) => {
      const currentPath = [...this.keys()].reduce(
        (memo, appRoute) => {
          const correctedAppRoute = appRoute.replace(/^\/?/, '/')
          const correctedMemo = memo ? memo.replace(/^\/?/, '/') : memo
          return this.startWithPath(path, correctedAppRoute) && this.startWithPath(correctedAppRoute, correctedMemo)
            ? appRoute
            : memo
        }, void 0
      )
  
      return (
        this.get(currentPath) || this.get('/') || {
          version: ''
        }
      )
    }
  
  }