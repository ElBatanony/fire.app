import "systemjs/dist/system";
import "systemjs/dist/extras/amd";
import "systemjs/dist/extras/named-register";
import "systemjs/dist/extras/named-exports";
import "systemjs/dist/extras/transform";
import { createBrowserHistory } from 'history'
import { Apps } from './apps'
import { defineVirtualModule } from './virtual-module'

const systemJSImport = async (requestUrl: string) => {
  const { default: component, mount, unmount } = await System.import(
    requestUrl
  )
  return { component, mount, unmount }
}

export default async ({ apps: rawApps, navigations, config }) => {
  defineVirtualModule({ navigations, config })

  const apps = new Apps(rawApps)

  const history = createBrowserHistory()

  let prevPathname = window.location.pathname

  const app = apps.findApp(history.location.pathname)

  const publicPath = `/${app.name}/${app.version}`

  __webpack_public_path__ = `${config.baseUrl}${publicPath}${__webpack_public_path__}`

  const appPath = `${config.baseUrl}${publicPath}/index.js`

  const { component, mount, unmount } = await systemJSImport(appPath);

  mount(component.default);

  history.listen((location) => {
    if (location.pathname !== prevPathname) {
      prevPathname = location.pathname
      unmount()
    }
  })
};
