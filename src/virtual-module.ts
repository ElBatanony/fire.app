const createVirtualModule = ({ config, navigations }) => ({
    getConfig: () => config,
    getConfigValue: (key) => config[key],
    getNavigations: () => navigations,
    getNavigationsValue: (key) => navigations[key],
})

export const defineVirtualModule = (params) => {
    const virtualModule = createVirtualModule(params)
    
    System.set('root.scope', {
        ...virtualModule
    });
}

